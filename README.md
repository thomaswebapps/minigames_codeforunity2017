# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is the Exported Project Files, built from Unity2017.  Download these files then Import as New Module into Android Studio
* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###
https://docs.google.com/document/d/1xXSAnfAxT_ICOn5vbVFziAmMcsCuZUjYKqgyK3tSiN0/edit?usp=sharing
(See link above to view images for instructions)


]The following steps will be used to modify the SalonID and ModuleID of the Minigame Project.  This also explains how to Build the project to import into the Client App.


Download the SalonCloudsMiniGame Unity Project
Open Unity 2017
Log into WebAppClouds Account:
Email- thomas@webappclouds.com
Pass- WebAppClouds63
Open the SalonCloudsMiniGame Unity Project
Open the GameSwitcher Scene file within Unity.
There will be a item called “SetID”.  Select this.
The Inspector, located on the right, will display the information of SetID.  This displays Salon ID and Module ID.  You can change these accordingly.  These values will affect all the minigames, telling the system which Salon Coupon to send if the user is a winner.

Once these values are set to the correct Salon and Module IDs.  You can save your changes and create a new Build.
Please Select File, then Build Settings.  The new window will have an option for Player Settings.  Please select this to choose the Keystore file needed to create a build.

The Keystore is available on the GoogleDrive folder “MiniGame Information and Files”.
The password for the keystore is “webappclouds”
Once this is completed, Select Export Project on the Build Settings window, then Select Build.
In Android Studio, you can open the current Client App and then Import Module.  Select the Unity Project to be the Module you are importing.
Set the new Lottery Button the current Client App to launch this imported module.
Save and Build the Android Studio Application and send to the QA Team for testing.


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###
thomas@webappclouds.com
daniel@webappclouds.com
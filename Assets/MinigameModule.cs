﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MinigameModule : MonoBehaviour{

	public int game_id { get; set; }
	public int salon_id { get; set; }
	public int module_id { get; set; }
	public string gift1 { get; set; }
	public string gift_1_image { get; set; }
	public string random1 { get; set; }
	public int status { get; set; }
	public string active_game { get; set; } //spinwheel, roulette, scratchoff, or cardgame
	public int game_code { get; set; }
	public int game_count { get; set; }
	public string inserted_date { get; set; }
	public string updated_date { get; set; }

	public void SetModule(int gID, int sID, int mID, string g1, string g1image, 
		string random1temp, int statustemp, string activeGame, int gcode, int gcount, 
		string insertedDate, string updatedDate)
	{
		game_id = gID;
		salon_id = sID;
		module_id = mID;
		gift1 = g1;
		gift_1_image = g1image;
		random1 = random1temp;
		status = statustemp;
		active_game = activeGame; //spinwheel, roulette, scratchoff, or cardgame
		game_code = gcode;
		game_count = gcount;
		inserted_date = insertedDate;
		updated_date = updatedDate;
	}

	public void Awake()
	{
		DontDestroyOnLoad (gameObject);
	}
}
	



	/*
[{"game_id":"1","salon_id":"185","module_id":"13723","gift1":"This is a test, congratulations on winning!"
,"gift_1_image":"","random1":"1","status":"1","active_game":"SpinWheel","game_code":"11","game_count":"0"
,"inserted_date":"2018-02-08 07:01:55","updated_date":"2018-02-08 11:43:41"}]
*/

	
	


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Text.RegularExpressions;

public class ScratchPostURL : MonoBehaviour {

	//https://saloncloudsplus.com/wslottery/lottery_winner/185/

	//post request
	public string url = "https://saloncloudsplus.com/wslottery/lottery_winner/";
	public string lottoIncreaseURL = "https://saloncloudsplus.com/wslottery/lottery/";
	public string sID;
	//index
	public bool enteredEmail = false;
	public string Useremail = "";
	public string Username = "Thomas";
	public string random_number = "WEB1SALGIFLOT";

	public InputField textEmail; 
	public GameObject InvalidBox;
	public GameObject SuccessBox;
	public GameObject WinnerBox;

	//character checker for email validation
	string MatchEmailPattern =
		@"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
                + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
                + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";

	//function for email validation to check above character pattern.  Call EmailValidation(email);
	bool EmailValidation(string email)
	{
		if (email != null) return Regex.IsMatch(email, MatchEmailPattern);
		else return false;
	}



	void Start()
	{
		sID = ChooseSalonID.instance.salonID.ToString();
	}

	//Increases Lottery Count
	public void increaseLottoCount()
	{
		StartCoroutine (UploadLottoCount ());
	}

	//Run this method to Send POST Request to the server
	public void SendCoupon()
	{
		if (enteredEmail) {
			WinnerBox.SetActive (false);
			StartCoroutine (Upload ());
			Debug.Log (url + sID + "/");
		} else {
			Debug.Log ("Please enter a valid email");
		}
	}

	public void EnteredEmail()
	{
		Useremail = textEmail.text.ToString ();

		Debug.Log(Useremail + " testing this");
		Debug.Log(EmailValidation (Useremail));
		if (EmailValidation(Useremail))
			{
			Debug.Log ("Valid email");
			enteredEmail = true;
		} else {
			Debug.Log ("Invalid email");
			InvalidBox.SetActive (true);
			Invoke ("disableInvalidBox", 3);
		}
	}
	void disableInvalidBox()	{	InvalidBox.SetActive (false);	}	//disables invalid email pop-up
	void disableSuccessBox()	{	
		WinnerBox.SetActive (false);
	}	//disables invalid email pop-up



	IEnumerator Upload()
	{
		WWWForm form = new WWWForm();
		form.AddField("email", Useremail);
		form.AddField("name", Username);
		form.AddField("random_number", random_number);

		//sends to url/id/ - should appear similar to https://saloncloudsplus.com/wslottery/lottery_winner/185/
		using (UnityWebRequest www = UnityWebRequest.Post(url + sID + "/", form))
		{
			yield return www.SendWebRequest ();

			//Failed 
			if (www.isNetworkError || www.isHttpError)
			{
				Debug.Log(www.error);
			}
			else
			//Successfully sent
			{
				SuccessBox.SetActive (true);
				Debug.Log("Post Request Sent!");
			}
		}
	}

	//increases the count of the lottery on the dashboard
	IEnumerator UploadLottoCount()
	{
		WWWForm form = new WWWForm();

		//sends to url/id/ - should appear similar to https://saloncloudsplus.com/wslottery/lottery_winner/185/
		using (UnityWebRequest www = UnityWebRequest.Post(lottoIncreaseURL + sID + "/", form))
		{
			yield return www.SendWebRequest ();

			//Failed 
			if (www.isNetworkError || www.isHttpError)
			{
				Debug.Log(www.error);
			}
			else
				//Successfully sent
			{
				Debug.Log("Lottery Count Increased!");
			}
		}
	}
}


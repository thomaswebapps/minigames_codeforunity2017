﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScratchOffManager : MonoBehaviour {

	public GameObject mask;
	public ScratchUIManager ui;
	public ScratchPostURL post;

	bool invoked = false;

	public void Update()
	{
		if (ui.canPlay) {
			if (Input.GetMouseButton (0)) {
				Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);

				Instantiate (mask, ray.origin, Quaternion.identity);
			}

			if (Input.GetMouseButtonDown (0)) {
				if (!invoked) {
					Invoke ("CheckOtherScriptForWinner", 5);
				}
				invoked = true;
			}
		}
	}

	void CheckOtherScriptForWinner()
	{
		ui.checkWinner ();
		post.increaseLottoCount ();
		Debug.Log ("launching lotto increase count");
	}
}

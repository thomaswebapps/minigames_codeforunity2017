﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScratchUIManager : MonoBehaviour {

	MinigameModule mm;
	GameObject minigameModule;
	public GameObject winnerBox;
	public GameObject loserBox;
	public GameObject AlreadyPlayedBox;
//	public GameObject ExitButton;
	public GameObject RevealAllMask;
	GameObject [] masks;

	public SpriteRenderer backgroundImage;
	public Sprite winnerBgr;
	public Sprite loserBgr;

	public string date;
	public bool canPlay;

	void Start()
	{
		CheckIfPlayedToday ();
		minigameModule = GameObject.FindWithTag ("module");
		mm = minigameModule.GetComponent<MinigameModule> ();

		if (mm.game_count >= mm.game_code) {
			backgroundImage.sprite = winnerBgr;
		} else {
			backgroundImage.sprite = loserBgr;
		}
	}

	//check if the user has won
	public void checkWinner()
	{
		SaveTheDate ();
		RevealAllMask.SetActive (true);

		if (canPlay) {
			if (mm.game_count >= mm.game_code) {
				//win
				winnerBox.SetActive(true);
				canPlay = false;
			} else {
				//lose
				loserBox.SetActive(true);
//				ExitButton.SetActive (true);
				canPlay = false;
			}
		}
		masks = GameObject.FindGameObjectsWithTag("masks");

		for (int i = 0; i < masks.Length; i++) {
			Destroy (masks [i]);
		}
	}

	//checks if the user has played today
	void CheckIfPlayedToday()
	{
		date = PlayerPrefs.GetString ("savedDate");		//load the last saved date to compare time for coupon use

		if (date == System.DateTime.Today.ToString()) {
			AlreadyPlayedBox.SetActive (true);
			canPlay = false;
//			ExitButton.SetActive (true);
//						canPlay = true;	//you can play (enable this line, comment others in section for demo)
		} else {
			canPlay = true;	//you can play
		}
	}

	//Save current date when ran.
	public void SaveTheDate()
	{
		Debug.Log (System.DateTime.Today);
		date = System.DateTime.Today.ToString();
		PlayerPrefs.SetString ("savedDate", date);
	}

	//enable the exit button
	public void EnableExitButton()
	{
		Debug.Log ("disable exit button");
//		ExitButton.SetActive (true);
	}

	//exits the minigame
	public void ExitApplication()
	{
		Application.Quit ();
	}
}

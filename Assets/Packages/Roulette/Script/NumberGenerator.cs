﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumberGenerator : MonoBehaviour {

	public int generatedNumber;

	// Use this for initialization
	public void GenerateNumber () 
	{
		generatedNumber = Random.Range(1,37);
	}
	
	// Update is called once per frame
	void Start () 
	{
		GenerateNumber ();
		Debug.Log (generatedNumber);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NumberManager : MonoBehaviour {

	public GameObject NumGenerator;
	public GameObject TextUI;
	public GameObject SpinButton;
	public GameObject textInputObj;
	public GameObject textInstructions;
	public GameObject textInstructionsInput;
	public GameObject AlreadyPlayedBox;
//	public GameObject ExitButton;
	public GameObject WinnerBox;
	public GameObject LoseBox;

	public string date;

	public int myNumber;
	public InputField textInput;

	public Text winText;

	public bool winner = false;
	public bool canPlay;

	NumberGenerator generatorScript;
	Animator anim;

	void Start()
	{
		generatorScript = NumGenerator.GetComponent<NumberGenerator>();
		anim = this.GetComponent<Animator> ();
		SpinButton.SetActive (false);
		winText.text = " ";
		CheckIfPlayedToday ();
	}

	// Use this for initialization
	public void setNumber () 
	{
		myNumber = int.Parse (textInput.text.ToString ());
		if (myNumber > 36) {
			myNumber = 36;
		} else if (myNumber < 1) {
			myNumber = 0;
		}
			
		textInput.text = myNumber.ToString();
			
		Debug.Log ("Number Set");
	}
	
	// Check if player has won
	public void CheckWin () 
	{
		SaveTheDate ();  //Saves the current date
		Debug.Log ("Checking Numbers" + myNumber + " vs " + generatorScript.generatedNumber);
		if (myNumber == generatorScript.generatedNumber) {
			winner = true;
		} else {
			winner = false;
		}

		//if user winds...
		if (winner) {
			//coupon
			WinnerBox.SetActive(true);
			winText.text = "You Win!";
			Debug.Log ("You Win");
			canPlay = false;
		} else {
			//Try again tomorrow...
			LoseBox.SetActive(true);
			winText.text = "You did not win." + "\n" + "Please try again tomorrow.";
			Debug.Log ("You Lose");
			canPlay = false;
			EnableExitButton ();
		}
	}

	//when SPIN button is pressed...
	public void Spin()
	{
		if (canPlay) {
			textInstructions.SetActive (false);
			textInstructionsInput.SetActive (false);
			textInputObj.SetActive (false);
			anim.SetBool ("spin", true);
			Invoke ("CheckWin", 5);
			SpinButton.SetActive (false);
			Invoke ("DisableSpin", 5);
		}
	}

	//Enable the SPIN button.  Once disabled, stays disabled for day.
	public void EnableSpin()
	{
		if(canPlay)
		SpinButton.SetActive (true);
	}

	void DisableSpin()
	{
		anim.SetBool ("spin", false);
	}

	public void EnableExitButton()
	{
//		ExitButton.SetActive (true);
	}


	void CheckIfPlayedToday()
	{
		date = PlayerPrefs.GetString ("savedDate");		//load the last saved date to compare time for coupon use
		if (date == System.DateTime.Today.ToString()) {
			AlreadyPlayedBox.SetActive (true);
			DisableSpin ();
			SpinButton.SetActive (false);
			textInstructions.SetActive (false);
			textInstructionsInput.SetActive (false);
			textInputObj.SetActive (false);
			canPlay = false;
//			ExitButton.SetActive (true);
//			canPlay = true;	//you can play (enable this line, comment others in section for demo)
		} else {
			canPlay = true;	//you can play
		}
	}

	//Save current date when ran.
	public void SaveTheDate()
	{
		Debug.Log (System.DateTime.Today);
		date = System.DateTime.Today.ToString();
		PlayerPrefs.SetString ("savedDate", date);
	}

	public void ExitApplication()
	{
		Application.Quit ();
	}
}

//set up email validator
//set up coupon call and Post Request
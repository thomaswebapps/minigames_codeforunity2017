﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ManagerTicker : MonoBehaviour {

	public int tickerNumber;

//	public Text tickerDisplay;
	public Text winText;
	public Text loseText;
	public Text AlreadyPlayedText;

	public GameObject winBox;
	public GameObject loseBox;
	public GameObject AlreadyPlayedBox;
	public GameObject SpinButton;
//	public GameObject ExitButton;
	public ManagerGame mg;
	public ManagerParticals mp;

	public string date;

	Animator anim;

	void Start()
	{
		anim = GetComponentInChildren<Animator> ();
		CheckIfPlayedToday ();
	}

	//Checks ticker value and flips ticker animation
	void OnTriggerEnter2D(Collider2D ticker)
	{
		for(int i = 0; i < 10; i++)
		{
			//ticker collides with 0-7
			if (ticker.gameObject.tag == i.ToString ())
			{
				tickerNumber = i;
			}
		}

		//if trigger tagged peg
		if (ticker.gameObject.tag == "peg") {
			Debug.Log ("flick");
			anim.SetBool("idle", false);
		}
	}

	//flips ticker animation to origin
	void OnTriggerExit2D(Collider2D tick)
	{
		if (tick.gameObject.tag == "peg") {
			anim.SetBool ("idle", true);
		}
	}

	//Checks win
	public void checkWin()
	{
		SaveTheDate ();  //Saves the current date

		if (tickerNumber == 4 || tickerNumber == 9) {
			//Winner
			winBox.SetActive(true);
//			tickerDisplay.enabled = false;
			anim.StopPlayback ();
			mp.PlayParticals ();	//Plays partical effects
			Debug.Log ("Winner winner chicken dinner");
		} else if (tickerNumber == 3 || tickerNumber == 6)	{
			//try again
			loseText.enabled = true;
			loseText.text = ("FREE SPIN!");
//			tickerDisplay.enabled = false;
			anim.StopPlayback ();
			Invoke ("Reset", 3);
			Debug.Log ("Spin again");
		} else {
			//lose
			loseBox.SetActive (true);
//			ExitButton.SetActive (true);
		}
	}

	//Set it so user can try 3 times
	public void Reset()
	{
		SpinButton.SetActive (true);
		mg.canSpin = true;
		loseText.text = ("Spin Again!");
		loseText.enabled = false;
//		tickerDisplay.enabled = true;
	}
		
	public void DisableSpinButton()
	{
		Debug.Log ("disable spin button");
		SpinButton.SetActive (false);
	}

	public void EnableExitButton()
	{
		Debug.Log ("disable exit button");
//		ExitButton.SetActive (true);
	}


	void CheckIfPlayedToday()
	{
		date = PlayerPrefs.GetString ("savedDate");		//load the last saved date to compare time for coupon use
		if (date == System.DateTime.Today.ToString()) {
			AlreadyPlayedBox.SetActive (true);
			DisableSpinButton ();
			mg.canPlay = false;
	//		mg.canPlay = true;	//you can play (enable this line, comment others in section for demo)
		} else {
			mg.canPlay = true;	//you can play
		}
	}

	//Save current date when ran.
	public void SaveTheDate()
	{
		Debug.Log (System.DateTime.Today);
		date = System.DateTime.Today.ToString();
		PlayerPrefs.SetString ("savedDate", date);
	}
}

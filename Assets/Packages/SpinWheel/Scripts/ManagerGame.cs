﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerGame : MonoBehaviour {

	public ManagerTicker mt;

	public bool canSpin = true;
	public bool canPlay = true;

	public void DelayDisableSpin()
	{
		Invoke ("DisableSpin", 1);
	}

	void DelayCheckWin()
	{
		Debug.Log ("check for win");
		mt.checkWin ();
	}

	void DisableSpin()
	{
		Debug.Log ("disabling");
		mt.DisableSpinButton ();
		if (canSpin && canPlay) {
			Invoke ("DelayCheckWin", 12);
			canSpin = false;
		}
	}

	public void ExitApplication()
	{
		Application.Quit();
	}
}

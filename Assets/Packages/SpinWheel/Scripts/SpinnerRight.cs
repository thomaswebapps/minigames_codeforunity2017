﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinnerRight : MonoBehaviour {

	public ManagerGame mg;

	public float rotationSpeed = 50;
	float rotateZ;
	public float autoSpinStart = 100;
	public float autoSpinEnd = 250;

	public GameObject Wheel;

	public Rigidbody2D rb;

	public Animator anim;

	void OnMouseDrag()
	{
		if (mg.canSpin && mg.canPlay) 
		{
			rotateZ = Input.GetAxis ("Mouse Y") * rotationSpeed * Mathf.Deg2Rad;

			rb.AddTorque (rotationSpeed * rotateZ);
		}
	}

	public void OnMouseDown()
	{
		mg.DelayDisableSpin();
		Invoke ("MinimumSpin", .9f);
	}

	//Set button to auto-spin with random torque
	public void SpinButton()
	{
		anim.SetBool ("right", true);
		float randomNum = Random.Range (autoSpinStart, autoSpinEnd);

		rb.AddTorque (randomNum);
	}

	//prevents user from using a slow spin to try to win
	void MinimumSpin()
	{
		Debug.Log (rb.angularVelocity);
		if (mg.canSpin && mg.canPlay) {
			if (rb.angularVelocity > 0) {
				float randomNum = Random.Range (50, 100);

				rb.AddTorque (randomNum);
				anim.SetBool ("right", true);
			} else {
				float randomNum = Random.Range (-50, -100);

				rb.AddTorque (randomNum);
				anim.SetBool ("right", false);
			}
		}
	}
}

﻿//simulate shuffle at start
//Move cards one at a time to slot positions.
//flip all cards over at end if loss.
//check winner, congratulations
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerMatchCards : MonoBehaviour {

	Animator anim;

	public GameObject DeckPrefab;

	public Transform[] cardSlots;
	public GameObject[] cards;
	public int[] cardElement;

	public Transform startMarker;
	public float speed = 1.0F;
	private float startTime;
	private float journeyLength;

	bool moveToSlots = false;

	public ManagerMatchGame mg;

	void Start() 
	{
		if(mg.canPlay == true)
		{
			anim = DeckPrefab.GetComponent<Animator> ();
			startTime = 0;
			journeyLength = Vector3.Distance(startMarker.position, cardSlots[0].position);
			DistributeCardsRandomly ();
			Invoke ("moveEnable", 5);	//Start after animation
		}
	}

	void Update() 
	{
		if(moveToSlots)
		{
			float distCovered = (Time.time - startTime) * speed;
			float fracJourney = distCovered / journeyLength;
			for (int i = 0; i < cards.Length; i++) 
			{
				cards[i].transform.position = Vector3.Lerp(startMarker.position, cardSlots[cardElement[i]].position, fracJourney);
			}
		}
	}

	void moveEnable()
	{
		startTime = Time.time;
		anim.enabled = false;
		moveToSlots = true;  //enable movement
		Invoke ("moveDisabled", 5);	//Start after animation
	}

	void moveDisabled()
	{
		startTime = 0;
		moveToSlots = false;  //disable movement
	}

	void DistributeCardsRandomly()
	{
		reshuffle (cardElement);	//shuffles the int values in the array
	}

	//Randomizes the value of each card slot.
	void reshuffle(int[] cardgoto)
	{
		// Knuth shuffle algorithm :: courtesy of Wikipedia :)
		for (int t = 0; t < cardgoto.Length; t++ )
		{
			int tmp = cardgoto[t];
			int r = Random.Range(t, cardgoto.Length);
			cardgoto[t] = cardgoto[r];
			cardgoto[r] = tmp;
		}
	}

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManagerMatchUI : MonoBehaviour {

	public Text attempts;
	public Text winText;
	public Text loseText;
	public Text shuffleText;

//	public GameObject ExitButton;
	public GameObject WinnerBox;
	public GameObject LoseBox;

	public InputField textInput;

	int attemptsLeft = 3;

	void Start()
	{
		Invoke ("disableShuffle", 5);
	}

	public void AttemptCount(int x)
	{
		attempts.text = "You have " + (attemptsLeft - x) + " tries remaining.";
	}

	void disableShuffle()
	{
		shuffleText.enabled = false;
		attempts.enabled = true;
	}

	public void winner()
	{
//		winText.enabled = true;
		WinnerBox.SetActive(true);
		attempts.enabled = false;
	}

	public void loss()
	{
		//loseText.enabled = true;
		attempts.enabled = false;
		//lose
		LoseBox.SetActive (true);
//		ExitButton.SetActive (true);
	}


	public void EnableExitButton()
	{
//		ExitButton.SetActive (true);
	}



}

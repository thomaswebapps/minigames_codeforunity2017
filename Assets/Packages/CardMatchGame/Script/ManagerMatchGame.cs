﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ManagerMatchGame : MonoBehaviour {

	public ManagerMatchCards mc;
	CardValue cardScript;
	public ManagerMatchUI mgrUI;

	public GameObject AlreadyPlayedBox;
//	public GameObject ExitButton;

	public string date;

	public bool canPlay;

	public int cardValueA = 0;
	public int cardValueB = 0;

	public int wincount = 0;
	public int losecount = 0;

	void Awake()
	{
		CheckIfPlayedToday ();
	}

	void Start()
	{
		Invoke ("Unlock", 7);
		DontDestroyOnLoad (this.gameObject);
	}

	void Unlock()
	{
		if (canPlay) {
			for (int i = 0; i < mc.cards.Length; i++) 
			{
				cardScript = mc.cards [i].GetComponent<CardValue> ();
				cardScript.UnlockCards ();
			}
		}
	}

	//if first slot has value, then give value to second slot
	public void flipCard(int x)
	{
		if (cardValueA == 0) {
			cardValueA = x;
		} else {
			cardValueB = x;
		}

		Debug.Log ("cardValue A = " + cardValueA + " and cardValue B = " + cardValueB);

		//if two cards are flipped... Check win...
		if (cardValueA != 0 && cardValueB != 0) 
		{
			Debug.Log ("Both values are not 0, checking for win");
			//for each card in cards array, lock cards so they cannot be flipped.
			for (int i = 0; i < mc.cards.Length; i++) 
			{
				cardScript = mc.cards [i].GetComponent<CardValue> ();
				cardScript.LockCards ();
			}
			CheckWin ();
		}
	}
	
	public void CheckWin () 
	{
		SaveTheDate ();  //Saves the current date

		if (cardValueA == cardValueB) 
		{
			//winner winner chicken dinner
			wincount++;
			cardValueA = 0;
			cardValueB = 0;

			if (wincount > 7) {
				//show coupon
				Debug.Log("Winner winner chicken dinner");
				mgrUI.winner ();
			} else {
				//check each card and unlock cards if they are not flipped.
				Unlock();
			}




		} else {
			//try again
			losecount++;
			mgrUI.AttemptCount (losecount);

			if (losecount > 2) 
			{
				//Game over man
				Debug.Log("Game Over");
				mgrUI.loss ();
				canPlay = false;
			} else 
			{
				//checks each card, flips unmatched cards back over to hidden position
				for (int i = 0; i < mc.cards.Length; i++) 
				{
					cardScript = mc.cards [i].GetComponent<CardValue> ();
					if (cardScript.value == cardValueA && cardScript.flipped == true) 
					{
						cardScript.UnFlip ();
					}
					if (cardScript.value == cardValueB && cardScript.flipped == true) 
					{
						cardScript.UnFlip ();
					}
				}

				//check each card and unlock cards if they are not flipped.
				Invoke ("Unlock", 2);
			}

			cardValueA = 0;
			cardValueB = 0;
		}
	}




	//checks if the user has played today
	void CheckIfPlayedToday()
	{
		date = PlayerPrefs.GetString ("savedDate");		//load the last saved date to compare time for coupon use

		if (date == System.DateTime.Today.ToString()) {
			AlreadyPlayedBox.SetActive (true);
			canPlay = false;
//			ExitButton.SetActive (true);
			//			canPlay = true;	//you can play (enable this line, comment others in section for demo)
		} else {
			canPlay = true;	//you can play
		}
	}

	//Save current date when ran.
	public void SaveTheDate()
	{
		Debug.Log (System.DateTime.Today);
		date = System.DateTime.Today.ToString();
		PlayerPrefs.SetString ("savedDate", date);
	}

	//exits the minigame
	public void ExitApplication()
	{
		Application.Quit ();
	}
}

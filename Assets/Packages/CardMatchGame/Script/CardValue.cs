﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardValue : MonoBehaviour {

	SpriteRenderer sr;

	public ManagerMatchGame mg;
	public Sprite[] currentImage;

	public int value;

	public bool flipped = false;
	public bool canFlip = false;

	bool flip = false;
	bool unflip = false;
	bool reveal = false;

	public Transform startAngle;
	public Transform finishAngle;

	private Vector3 currentAngle;

	float timer = 0;

	void Start()
	{
		sr = GetComponent<SpriteRenderer> ();

		currentAngle = transform.eulerAngles;
	}

	//when clicked (add, when touch)
	void OnMouseDown()
	{
		if (canFlip) 
		{
			flipped = true;
			mg.flipCard (value);
			Flip ();

			Debug.Log ("flipping card with value " + value);
			canFlip = false;
		}
	}

	void Update()
	{
		if (flip) {
			//timer for flipping
			timer = timer + Time.deltaTime;
			//animates flip
			currentAngle = new Vector3 (
				Mathf.LerpAngle (0, 0, 0),
				Mathf.LerpAngle (0, 89, timer * 3),
				Mathf.LerpAngle (0, 0, 0));
			//tells current object to change to above values
			transform.eulerAngles = currentAngle;
		} 
		else if (unflip) {
			//timer for flipping
			timer = timer + Time.deltaTime;
			//animates flip
			currentAngle = new Vector3 (
				Mathf.LerpAngle (0, 0, 0),
				Mathf.LerpAngle (0, 89, timer * 3),
				Mathf.LerpAngle (0, 0, 0));
			//tells current object to change to above values
			transform.eulerAngles = currentAngle;
		} 
		else if (reveal) {
			//timer for flipping
			timer = timer + Time.deltaTime;
			//animates flip
			currentAngle = new Vector3 (
				Mathf.LerpAngle (0, 0, 0),
				Mathf.LerpAngle (89, 0, timer * 3),
				Mathf.LerpAngle (0, 0, 0));
			//tells current object to change to above values
			transform.eulerAngles = currentAngle;
		}
	}

	//Flip card
	public void Flip()
	{
		flip = true;
		Invoke ("RevealCard", .33f);

		Debug.Log("Flipping card");
	}

	//Runs half second after Flip() method, moves card angle back to 0 and changes image to match value
	void RevealCard()
	{
		DisableFlip ();
		//if value, then sprite == new image (if unflipped, go back to original sprite
		if (flipped) {
			for (int i = 0; i < currentImage.Length; i++) {
				if (value == i) {
					sr.sprite = currentImage [i];
				}
			}
		} else {
			sr.sprite = currentImage [0];
		}
		reveal = true;
		Invoke ("DisableReveal", 1);

		Debug.Log("Reveal card");
	}

	void DisableFlip()		{
		flip = false;
		timer = 0;
	}
	void DisableReveal()	{
		reveal = false;
		timer = 0;
	}
	void DisableUnflip()	{
		unflip = false;
		timer = 0;
	}

	//Card did not match, flip back to original image
	public void UnFlip()
	{
		Invoke ("delayUnflip", 2);
	}
	//Gives user time to see incorrect cards
	void delayUnflip()
	{
		unflip = true;

		flipped = false;
		Invoke ("DisableUnflip", .5f);
		Invoke ("RevealCard", .6f);

		Debug.Log("Unflipping card");
	}

	//No cards can be flipped
	public void LockCards()
	{
		canFlip = false;
	}

	//unflipped cards can now be flipped over
	public void UnlockCards()
	{
		if (flipped == false) 
		{
			canFlip = true;
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;
using SimpleJSON;
using System;

[System.Serializable]

public class GetURL : MonoBehaviour {

	public string url = "https://saloncloudsplus.com/wsgames/games_list_bysalonid/";
	public string sID = "";
	public string moduleID = "";

	public MinigameModule mm;
	public GameSwitcher scene;

	ChooseSalonID CSID;
	ChooseModuleID CMID;
	BroadcastReceiver BR;
	public GameObject IDchooser;
	public GameObject BroadcastReceiverOBJ;
	public GameObject NoNetworkBox;

	void Awake()
	{
		CSID = IDchooser.GetComponent<ChooseSalonID> ();
		CMID = IDchooser.GetComponent<ChooseModuleID> ();
		BR = BroadcastReceiverOBJ.GetComponent<BroadcastReceiver> ();
	}

	void Start()
	{
		sID = CSID.salonID.ToString ();
		moduleID = CMID.ModuleID.ToString ();

		Debug.Log (BR.javaMessage.Length);
		if (BR.javaMessage.Length > 7) 
		{
			sID = BR.javaMessage.Substring (0, 2);
			moduleID = BR.javaMessage.Substring (4, 8);
		}
		StartCoroutine(GetText());
	}

	IEnumerator GetText()
	{
		using (UnityWebRequest www = UnityWebRequest.Get(url + sID + "/" + moduleID))
		{
			yield return www.SendWebRequest();

			if (www.isNetworkError || www.isHttpError)
			{
				Debug.Log(www.error);
				Debug.Log ("Error - cannot load scene due to connection failure");
				NoNetworkBox.SetActive (true);
			}
			else
			{
				// Show results as text
				Debug.Log(www.downloadHandler.text);
				string jsonResponse = www.downloadHandler.text;

				//using simpleJSON
				var jsonObj = JSON.Parse (jsonResponse);

				//Set values into MinigameModule script
				mm.SetModule (
						Convert.ToInt32(jsonObj [0] ["game_id"].Value), 
						Convert.ToInt32(jsonObj [0] ["salon_id"].Value), 
						Convert.ToInt32(jsonObj [0] ["module_id"].Value), 
						jsonObj [0] ["gift1"].Value,
						jsonObj [0] ["gift_1_image"].Value, 
						jsonObj [0] ["random1"].Value, 
						Convert.ToInt32(jsonObj [0] ["status"].Value),
						jsonObj [0] ["active_game"].Value, 
						Convert.ToInt32(jsonObj [0] ["game_code"].Value), 
						Convert.ToInt32(jsonObj [0] ["game_count"].Value),
						jsonObj [0] ["inserted_date"].Value, 
						jsonObj [0] ["updated_date"].Value);


				scene.SelectScene ();


				/*
[{"game_id":"1","salon_id":"185","module_id":"13723","gift1":"This is a test, congratulations on winning!"
,"gift_1_image":"","random1":"1","status":"1","active_game":"SpinWheel","game_code":"11","game_count":"0"
,"inserted_date":"2018-02-08 07:01:55","updated_date":"2018-02-08 11:43:41"}]
*/
			}
		}
	}
}
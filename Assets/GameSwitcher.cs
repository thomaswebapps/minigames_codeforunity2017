﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameSwitcher : MonoBehaviour {

	public MinigameModule mm;

	public void SelectScene()
	{
		if (mm.active_game == "SpinWheel") {
			SceneManager.LoadScene (1);
		}

		else if (mm.active_game == "Roulette") {
			SceneManager.LoadScene (2);
		}

		else if (mm.active_game == "CardMatcher") {
			SceneManager.LoadScene (3);
		}

		else if (mm.active_game == "Scratch-Off") {
			SceneManager.LoadScene (4);
		}
	}
}
